function calculateRanges(ranges, target) {
  return ranges.reduce((result, curr) => {
    if (target >= curr[0] && target < curr[1]) {
      return (result += 1);
    }
    return result;
  }, 0);
}

module.exports = calculateRanges;
