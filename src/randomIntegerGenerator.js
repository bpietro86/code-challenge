const MAX_VALUE = 1000000000;

function randomNumberGenerator(diff = 0) {
  return Math.floor(Math.random() * MAX_VALUE - diff + 1);
}

module.exports = randomNumberGenerator;