const minimist = require("minimist");

module.exports = (() => {
  const args = minimist(process.argv.slice(2));
  let cmd = args._[0] || "help";

  if (args.version || args.v) {
    cmd = "version";
  }

  if (args.help || args.h) {
    cmd = "help";
  }

  switch (cmd) {
    case "challenge":
      if (args["ranges"]) {
        ranges = args["ranges"];
        if (!Number.isInteger(ranges)) {
          throw new Error("ranges must be an integer");
        }
        if (ranges > 1000000000) {
          throw new Error("number of ranges must be <= 1000000000");
        }
      }
      require("./app")(args);
      break;
    case "help":
      require("../cmds/help")(args);
      break;
    case "version":
      require("../cmds/version")(args);
      break;
    default:
      throw new Error('Wrong command.')
  }
})();