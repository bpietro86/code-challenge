const calculateRanges = require('../src/calculateRanges')

function getResult(randomNumber) {
  const ranges = [
    [10, 60],
    [0, 90],
    [95, 102]
  ];

  // return Math.min(minIndex + 1, maxIndex + 1);
  return calculateRanges(ranges, randomNumber);
}

test("assignment example", () => {
  // fake loop
  expect(getResult(65)).toBe(1);

  expect(getResult(11)).toBe(2);

  expect(getResult(0)).toBe(1);

  expect(getResult(94)).toBe(0);

  expect(getResult(50)).toBe(2);
});