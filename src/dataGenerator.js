const randomIntegerGenerator = require("./randomIntegerGenerator");

function getRandomRange() {
  const min = randomIntegerGenerator();
  const max = randomIntegerGenerator(min) + min;
  return [min, max];
}

function dataGenerator(numOfRanges = 1000000) {
  const ranges = [];
  for (var i = 0; i < numOfRanges; i++) {
    ranges.push(getRandomRange());
  }
  return ranges;
}

module.exports = dataGenerator;