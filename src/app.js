const os = require("os");
const ora = require("ora");
const readline = require("readline");
const cluster = require("cluster");
const randomIntegerGenerator = require("./randomIntegerGenerator");
const dataGenerator = require("./dataGenerator");
const calculateRanges = require("./calculateRanges");
const logger = require('./logger');

const numOfCPUs = os.cpus().length;

const spinner = ora("Generating results (ctrl+t to exit)")
spinner.color = "green";

readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);

process.stdin.on('keypress', (key, data) => {
  if (data.ctrl && data.name === 't') {
    process.exit(0);
  }
});

module.exports = args => {
  const numberOfRanges = args.ranges || 1000000;
  const ranges = dataGenerator(numberOfRanges);

  if (cluster.isMaster) {
    spinner.start();
    for (let i = 0; i < numOfCPUs; i++) {
      const worker = cluster.fork();
      worker.send(randomIntegerGenerator());

      worker.on("message", () => {
        worker.send(randomIntegerGenerator());
      });
    }

    cluster.on("exit", function (worker, code, signal) {
      cluster.fork();
    });
  } else if (cluster.isWorker) {
    process.on("message", randomNumber => {
      const result = calculateRanges(ranges, randomNumber);
      logger.log('info', {
        message: `${randomNumber} => Enclosed by ${result} range(s)`
      });
      process.send("ok");
    });
  }
};