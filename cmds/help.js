const menus = {
  main: `
    npm start [command] <options>

    challenge .......... run the assignment
    version ............ show package version
    help ............... show help menu for a command`,

  challenge: `
    npm start challenge <options>
    --ranges, -r ..... number of ranges to generate, up to 1000000000`
};

module.exports = args => {
  const subCmd = args._[0] === "help" ? args._[1] : args._[0];

  console.log(menus[subCmd] || menus.main);
};