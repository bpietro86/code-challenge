# EF CODE CHALLENGE

## Search algorithm

I ended up with the simplest solution, by iterating the array of ranges to check if a number is included or not, with a linear complexity O(n).

I wanted to split the generator of random ranges into threads but since node.js is mono thread I couldn’t do it.

The solution with cluster is more complex since you need to keep syncronize all workers results.

## CPU Cluster for better performance

I used node cluster to parallelize the generation of random numbers and the search through ranges.

Performance
The performance are good for **N <= 1,000,000**.

## How to run the app

- npm install
- npm start challenge —ranges \$numberOfRanges
- _npm start help_
- The output results of the app are saved on a **output.log** file
